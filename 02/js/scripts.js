var arr = new Array();

function Person(firstName, lastName){

    this.fName = firstName;
    this.lName = lastName; 
    
    this.sayHello = function(){
        return this.fName + " " + this.lName;
    };  
    //-> to nie jest to samo co z return poniżej:

    // return {
    //     fName: firstName,
    //     lName: lastName
    // }; - w tym przypadku przy tworzeniu instancji nie musimy dodawać "new"
}

// To zwróci na obiekt:
var person1 = new Person("Jan", "Kowalski");
var person2 = new Person("Tomasz", "Kowalski")

//To zwróci undefind:
var person3 = Person();
  