/*

Shape
    Rectangle
        Square
    Triangle

 */


 //tworzymy konstruktor Shape czyli po prostu funkcję:
 function Shape(sideLengths){
     this._name = "";
     this._sideLengths = sideLengths;

 }

 Shape.prototype.getPerimeter = function(){

    return this._sideLengths.reduce(function(preVal, val){
        return preVal + val;
    });
 }; 

 Shape.prototype.getArea = function(){
     return this._sideLengths[0] * this._sideLengths[1];
 };

function Rectangle(sideLengths) {
    Shape.call(this, [sideLengths[0], sideLengths[1], sideLengths[0], sideLengths[1]]);
    //pobralismy okreslenie parametrów z Shape, daliśmy nasze parametry
    //to jeszcze nie jest dziedziczenie tylko jakby pożyczka (pożyczenie konstruktora).

    //teraz jeszcze nadpisujemy _name:
    this._name = "Prostokąt";

}

function Square(sideLength){
    Rectangle.call(this, [sideLength, sideLength]);

    this._name ="Kwadrat";
}





 var shape1 = new Shape([20, 20, 20, 20]);
 
 var shape2 = new Rectangle([20, 10]);

 var shape3 = new Square(10);

 //nie powinnimy tego robić:
 //jeśli chcemy name to powinniśmy stworzyć metodę getName:
 //shape1._name; .
 //jeśli chcemy name to powinniśmy stworzyć metodę getName