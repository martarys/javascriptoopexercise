// function Person(firstName, lastName) {
//     this.fName = firstName;
//     this.lName = lastName;

//     this.sayHello = function() {
//         return this.fName + " " + this.lName;
//     };
// }

// var person1 = new Person("Jan", "Kowalski");
// var person2 = new Person("Tomasz", "Nowak");

//przy takim kodzie jw, za każdym wywołaniem instancji tworzymy 
// metodę sayHello. Lepiej by było, żebu utworzyć tę metodę tylko raz
//i żeby wszystkie obiekty tej instancji z niej korzystały

//Robimy to przez przypisanie do prototypu. Wtedy metoda nie jest 
//dostępna bezpośrednio na obiektach utworzonych z tej klasy:

function Person(firstName, lastName) {
        this.fName = firstName;
        this.lName = lastName;
    
    }
    
    // Object.prototype.sayHello = function() {
    //     return this.fName + " " + this.lName;
    // };

    Person.prototype.sayHello = function() {
         return this.fName + " " + this.lName;
         };

    var person1 = new Person("Jan", "Kowalski");
    var person2 = new Person("Tomasz", "Nowak");

    person1.sayHello = function(){
        return this.fName.toUpperCase() + " " + this.lName.toUpperCase();
    };

    // var arr = [];

    // arr.fName = "Tablica";
    // arr.lName = "Nowakowska";
