// function sayHello() {
//     console.log("Cześć!");
// }

function sayHello(text){
    console.log(this.firstName + " " + this.lastName);
}

var person = {
    firstName: "Jan",
    lastName: "Kowalski",
};

// 
sayHello.call(person);

//pobieramy elementy listy z html do zmiennej:

var elems = document.querySelectorAll("#list li");

//chcemy użyć forEach dla elems, który nie ma tej funkcji, bo jest tablicopodobny.
//forEach oprócz this przyjmuje funkcję jako parametr:

Array.prototype.forEach.call(elems, function(elem){
    elem.style.color = "red";
});

//można zamiast Array.prototype.forEach po prostu utworzyć nową tablicę:
//[].forEach etc - krótszy zapis, ale mniej wydajne

function sum(first, second, third){
    console.log(first + second + third);
}

sum(2, 3, 4);

sum.apply(this, [2, 3, 70] );