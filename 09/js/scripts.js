function Shape(sideLenghts){

    //warunek, żeby this nas kierowało na właściwy obiekt:

    if(!(this instanceof Shape)){
        return new Shape(sideLenghts);
    }
    this.sideLenghts = sideLenghts;
}

function sayHello() {

    //w funkcji globalnej "this" dotyczy obiektu globalnego - tu - window:
    console.log(this);
    return this.firstName + " " + this.lastName;
}

var firstName = "Jan",
    lastName = "Kowalski"; 

var person = {
    firstName: "Jan",
    lastName: "Kowalski",

    //referencja do funkcji, która jest wyżej:
    sayHello: sayHello
};

var person2 = {
    firstName: "Ala",
    lastName: "Ryś",
    sayHello: sayHello
};


//Jesli nie utworzymy nowego obiektu - new przed Shape w:
//var shape =  Shape([20, 20, 20, 20]); - to właściwość sideLenghts zostanie przypisana
//do obiektu globalnego - window - a chcieliśmy stworzyć nowy obiekt. 
//Stało sie tak przez użycie this w funkcji Shape.
//Możemy to naprawić dodając warunek w funkcji Shape jw.


var shape = new Shape([20, 20, 20, 20]);