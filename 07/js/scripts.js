//Ponieważ forEach jest dostępny już w NodeList, chciałabym dopisać funkcję 
//toUpperCase do NodeList - na razie nie umiem tego.

//NodeList.prototype.toUpperCase = function(){
//     this.forEach(function(elem){
//         elem.toUpperCase();
//     });
// }

//w kursie jest takie rozszerzenie:

NodeList.prototype.forEach = function(callback){

    for(var i = 0; i < this.length; i++){
        callback(this[i]);
    }
};


var elems = document.querySelectorAll("#list li"); //new NodeList

elems.forEach(function(elem){
    elem.style.color = "#ff0000";
});