function Person(firstName, lastName, age){
    this.firstName = firstName;
    this.lastName = lastName;
    this.age = age;
}

Person.prototype.sayHello = function(){
    return "Hi, " + this.firstName + " " + this.lastName;
};

var person = {
    firstName: "Jan",
    lastName: "Kowalski",
    age: 0
};

var person2 = new Person("Anna", "Nowak", 48);


if(person.lastName){
    console.log(person.age);
}


//operator in
if("age" in person){
    console.log(person.age);
}

if("forEach" in NodeList.prototype){
    console.log(true);
}


//hasOwnProperty
for(var key in person2){
    
    if(person2.hasOwnProperty(key)){
        
        console.log(key);
    }

}

