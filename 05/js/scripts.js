/*

Shape
    Rectangle
        Square
    Triangle

 */

function Shape(sideLengths) {
    this._name = "";
    this._sideLengths = sideLengths ;
}

Shape.prototype.getPerimeter = function() {

    return this._sideLengths.reduce(function(prevVal, val) {
        return prevVal + val;
    });

};

Shape.prototype.getArea = function() {
    return this._sideLengths[0] * this._sideLengths[1];
};

function Rectangle(sideLengths) {
    Shape.call(this, [sideLengths[0], sideLengths[1], sideLengths[0], sideLengths[1]]);

    this._name = "Prostokąt";
}


function Square(sideLength) {
    Rectangle.call(this, [sideLength, sideLength]);

    this._name = "Kwadrat";
}

//teraz chcemy, żeby prototyp Rectangle i Square dziedziczyli prototyp Shape
//wciąż nie jest to prawdziwe dziedziczenie:
Rectangle.prototype = Object.create(Shape.prototype);

//po przypisaniu prototypu Rectangle do nowego obiektu dziedziczącego 
// po prototypie Shape, musimy na nowo okreslić konstruktor Rectangle:
Rectangle.prototype.constructor = Rectangle;

Square.prototype = Object.create(Rectangle.prototype);
Square.prototype.constructor = Square;

function Triangle(sideLength) {
    Shape.call(this, [sideLength, sideLength, sideLength]);

    this._name = "Trójkąt równoboczny";

    
}

Triangle.prototype = Object.create(Shape.prototype);
Triangle.prototype.constructor = Triangle;

//UWAGA: zapis poniżej modyfikuje cały prototyp Shape, 
//bo Triangle.prototype = Shape.prototype:

Triangle.prototype.getArea = function(){
    var a = this._sideLengths[0];

    //Math.sqrt(3) to po prostu poierwiastek z 3
    return +( ( a * a * Math.sqrt(3)) /4).toFixed(2);
}

var square = new Square(20);

var triangle = new Triangle(20);

var rectangle = new Rectangle([20, 10]);