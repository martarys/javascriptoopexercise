function sayHello(text) {
    console.log(text + " " + this.firstName + " " + this.lastName);
}

var person = {
    firstName: "Jan",
    lastName: "Kowalski"
};


//kopia funkcji sayHello z ustawionym this
var hello = sayHello.bind(person, "Witaj");


//jak można samemu zrobić bind:
function bind(fn, obj){

    var args = Array.prototype.slice.call(arguments, 2);
    console.log(args);

    return function(){
        fn.apply(obj, args);
    };
}

var hello2 = bind(sayHello, person, "Witaj ");